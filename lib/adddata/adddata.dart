import 'dart:async';
import 'package:flutter/material.dart';
import 'package:gsheets/gsheets.dart';
import 'package:medicalgas/adddata/googlesheets.dart';
import 'package:medicalgas/adddata/sheetscolumn.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:medicalgas/scanner/qr_scan_full_screen.dart';
import 'package:rxdart/rxdart.dart';
import 'package:searchfield/searchfield.dart';

class adddata extends StatefulWidget {
  final String qrData;
  adddata({
    required this.qrData,
  });

  @override
  State<adddata> createState() => _adddataState();
}

class _adddataState extends State<adddata> {
  DateTime _dateTime = DateTime.now();
  sheetscolumn data = sheetscolumn();

  TextEditingController snController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();
  TextEditingController InstrumentController = new TextEditingController();

  final BehaviorSubject<List<String>> _listQrDataObs =
      BehaviorSubject<List<String>>();
/////////////////////////////////////
  List<String> Intrument = ["ไม่มี", "ชุด Cannula", "Regulator", "รถเข็น"];

/////////////////////////////////////
  List<String> places = [
    "RFS",
    "Outsource",
    "Logistics",
    "Zone A",
    "Zone B",
    "Zone C",
    "Zone D",
    "Zone E",
    "Zone F",
    "Zone H",
    "Zone J",
    "Zone K",
    "Zone L",
    "Zone N",
    "Zone O",
    "Zone Q",
    "Check Up Center",
    "หน่วยตรวจสุขภาพ",
    "Dental Care Center",
    "ศูนย์ทันตกรรม",
    "Zone R",
    "Zone S",
    "Zone T",
    "Zone U",
    "Zone V",
    "Endoscopy Center",
    "ศูนย์ส่องกล้อง",
    "Short stay",
    "หน่วยบำบัดระยะสั้น",
    "Hemodialysis",
    "หน่วยไตเทียม",
    "ICU 51",
    "Chemotherapy Service",
    "หน่วยบริการเภสัชกรรมด้านยาเคมีบำบัด",
    "LR",
    "ห้องคลอด",
    "Nursery",
    "หน่วยบริบาลทารกแรกเกิด",
    "Ward 62",
    "Ward 63",
    "Ward 64",
    "Ward 65",
    "NICU",
    "Sleep Lab",
    "Ward 71",
    "Ward 72",
    "Ward 73",
    "Ward 74",
    "Ward 75",
    "Ward 76",
    "Rehabilitation Center",
    "ศูนย์เวชกรรมฟื้นฟู",
    "Ward 81",
    "Ward 82",
    "Ward 83",
    "Ward 84",
    "Ward 85",
    "Ward 86",
    "CCU 91",
    "ICU 92",
    "ICU 95",
    "Ward 96",
    "Ambulatory ECG Monitoring Center&Fluoroscopy",
    "ศูนย์ติดตามคลื่นไฟฟ้าหัวใจและหัตถการฟลูโอโรสโคปี้",
    "Presidential Suite",
    "ห้องพักพิเศษ",
    "Royal Suite",
    "ที่ประทับส่วนพระองค์"
  ];
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    SheetsFlutter.init();
    _listQrDataObs.sink.add([widget.qrData]);
    snController.text = widget.qrData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.green,
            title: Text(
              "สถานะปัจจุบัน",
              style: TextStyle(fontSize: 30),
            )),
        body: Center(
            child: Padding(
                padding: const EdgeInsets.all(20),
                child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 20.0,
                          ),

//////////////////////SN///////////////////////////// Text('${widget.qrData}'), ////////////////////////////////
//                           Padding(
//                             padding: const EdgeInsets.symmetric(vertical: 8),
//                             child: TextFormField(
//                               controller: snController,
//                               decoration: InputDecoration(
//                                 filled: true,
//                                 fillColor: Colors.grey[350],
//                                 border: OutlineInputBorder(
//                                   borderRadius: BorderRadius.circular(8.0),
//                                   borderSide: BorderSide.none,
//                                 ),
//                                 labelText: "เลข SN",
//                               ),
//                             ),
//                           ),

                          StreamBuilder<List<String>>(
                              stream: _listQrDataObs.stream,
                              initialData: [widget.qrData],
                              builder: (context, snapshot) {
                                return ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: snapshot.data!.length,
                                    itemBuilder: (context, index) {
                                      return Column(
                                        children: [
                                          Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8,
                                                      vertical: 8),
                                                  decoration: BoxDecoration(
                                                      color: Colors.grey[350],
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8)),
                                                  child: Text(
                                                      snapshot.data![index]),
                                                ),
                                              ),
                                              IconButton(
                                                  onPressed: () {
                                                    if (_listQrDataObs
                                                            .value.length ==
                                                        1) {
                                                      Navigator.of(context)
                                                          .pop();
                                                    } else {
                                                      final listData =
                                                          _listQrDataObs.value;
                                                      listData.removeAt(index);
                                                      _listQrDataObs.sink
                                                          .add(listData);
                                                    }
                                                  },
                                                  icon: const Icon(
                                                    Icons.dangerous,
                                                    color: Colors.red,
                                                  ))
                                            ],
                                          ),
                                          index == snapshot.data!.length - 1
                                              ? Row(
                                                  children: [
                                                    GestureDetector(
                                                        onTap: () {
                                                          Navigator.of(context)
                                                              .push(MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          const QrScanFullScreen()))
                                                              .then((value) {
                                                            Timer(
                                                                const Duration(
                                                                    milliseconds:
                                                                        300),
                                                                () {
                                                              if (value is Map<
                                                                  String,
                                                                  dynamic>) {
                                                                final listData =
                                                                    _listQrDataObs
                                                                        .value;
                                                                final dataQuery = listData
                                                                    .where((element) =>
                                                                        element ==
                                                                        value[
                                                                            'qrData'])
                                                                    .toList();
                                                                if (dataQuery
                                                                    .isEmpty) {
                                                                  listData.add(
                                                                      value[
                                                                          'qrData']);
                                                                  _listQrDataObs
                                                                      .sink
                                                                      .add(
                                                                          listData);
                                                                } else {
                                                                  Fluttertoast
                                                                      .showToast(
                                                                          msg:
                                                                              'QrCode ซ้ำกัน');
                                                                }
                                                              }
                                                            });
                                                          });
                                                        },
                                                        child: const Icon(
                                                          Icons.add_circle,
                                                          color: Colors.green,
                                                        )),
                                                    const Text(
                                                        'เพิ่ม ID NUMBER'),
                                                  ],
                                                )
                                              : Container()
                                        ],
                                      );
                                    });
                              }),

//////////////////////Location/////////////////////////////
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: SearchField(
                                suggestions: places
                                    .map((e) => SearchFieldListItem(e))
                                    .toList(),
                                controller: locationController,
                                itemHeight: 40,
                                suggestionState: Suggestion.expand,
                                textInputAction: TextInputAction.next,
                                hasOverlay: false,
                                validator: (x) {
                                  if (!places.contains(x) || x!.isEmpty) {
                                    return 'กรุณาเลือกสถานที่ให้ตรงคำแนะนำ';
                                  }
                                  return null;
                                },
                                onSuggestionTap: (x) {},
                                searchStyle: TextStyle(color: Colors.black),
                                searchInputDecoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.grey[350],
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    borderSide: BorderSide.none,
                                  ),
                                  labelText: "เลือกตำแหน่ง",
                                ),
                              ),
                            ),
                          ),
///////////////////////////////////Instrument////////////////////////////////
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: SearchField(
                                suggestions:
                                    Intrument.map((e) => SearchFieldListItem(e))
                                        .toList(),
                                controller: InstrumentController,
                                itemHeight: 40,
                                suggestionState: Suggestion.expand,
                                textInputAction: TextInputAction.next,
                                hasOverlay: false,
                                validator: (y) {
                                  if (!Intrument.contains(y) || y!.isEmpty) {
                                    return 'กรุณาเลือกอุปกรณ์ให้ตรงคำแนะนำ';
                                  }
                                  return null;
                                },
                                onSuggestionTap: (y) {},
                                searchStyle: TextStyle(color: Colors.black),
                                searchInputDecoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.grey[350],
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    borderSide: BorderSide.none,
                                  ),
                                  labelText: "อุปกรณ์เพิ่มเติม",
                                ),
                              ),
                            ),
                          ),

/////////////////////////save/////////////////////////////
                          GestureDetector(
                            onTap: () async {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                try {
                                  List<Map<String, dynamic>> data = [];
                                  for (String item in _listQrDataObs.value) {
                                    data.add({
                                      sheetscolumn.ID: item,
                                      sheetscolumn.location:
                                          locationController.text.trim(),
                                      sheetscolumn.Instrument:
                                          InstrumentController.text.trim(),
                                    });
                                  }

                                  await SheetsFlutter.insert(data);

                                  for (String item in _listQrDataObs.value) {
                                    await SheetsFlutter.update(item, data);
                                  }

                                  Fluttertoast.showToast(
                                      msg: "บันทึกเรียบร้อย",
                                      gravity: ToastGravity.TOP);
                                  _formKey.currentState!.reset();
                                } on GSheetsException {}
                              }
                            },
                            child: Container(
                              height: 70,
                              width: 400,
                              color: Colors.green,
                              child: Center(
                                  child: Text(
                                "บันทึก",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              )),
                            ),
                          ),
                        ],
                      ),
                    )))));
  }
}
