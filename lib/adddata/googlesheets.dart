import 'dart:async';
import 'package:gsheets/gsheets.dart';
import 'package:medicalgas/adddata/sheetscolumn.dart';

class SheetsFlutter {
  static String _sheetId = "1b-jGpDBouTt6bF0iOL7U2pTfCMkFd4XY143GxM451yc";
  static const _sheetCredentials = r'''{

"type": "service_account",
"project_id": "gsheets-374213",
"private_key_id": "af6aa4c3e7e561834da90e28647a0b3eeb876359",
"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCxKu+AZWTw4VJh\nQR7LYE4PuNC1+pmK5xYuh0I2zxJXDPn16VwDmQ9y7JkFs5XHsG5vI3TbU+BZXc3C\nBLHcY6hgXREakbVyrZOnBKH7omwUJN+aOAHBEtQnMyQlAHW4le+jabDMoXLERdYx\n1EEn6kdsOs67ixeIOJ2X8jO3FUZkWO+3pzbi+0QrhadzoUCcW5ZW5u4uHl4kFS9Z\nDOrvvK+eMO389YFt3kZd1nXRhmZvdaw0yA0Pp0JACM0wKBsHHbaVVaRPnR+IpITu\nWTLoV/WagYRJ5KiGKF6iAc67Kxntlx2+8otOoIXZvs6ABcTj6x/hVgd8MPqdSBPz\nFEB1gkxXAgMBAAECggEAK3NVQ88aP31zX8jBza0z1+VCwq7QQ2HDbevcUGben5ze\nHPJ7j6JVSKFAMkbGADCEFRvECBp9Fk3MLb5LBLsoS2Hn1PVRY+uwH7EiLP27tgEJ\nGeCMhZahk2+WeMCxHnEfUZuVMcqSxBJAov3kcO6jT6boltRZFB6Wf/J1vnwoRGmm\n/oXqqIhDv5TO8UlRgdtsqO9C7SfzropBjmrCFPlJgx+7I6HKTJCcazSS0oKP9y0G\n9/1vyn4EnawcEn5kUktDIHwOhulu6xEwyzbK6Ui6ec41aMMkJUmRKknFByRMY3JM\ndn8qI/TtpV7Ijy/2lYPLmg6g+s7/2paaybO7PzLvMQKBgQDYxQRifPf27npVLH0h\nq3cjOozoFb8kzMkZksuc1G9KNqDrszjNrsoKDZmzelS11UfAmVGd1+knMJUMMZqb\nO7F/HCskhIWMzZsAkpp09Ff0e1QmkzwUtIaSiSyHe9qaUHpNw0Lk27PxQUJGnkJ1\n8h17vpdGfq2P/9yfkGnjoipHKQKBgQDROyeUwvthoL2OSmLYkjzCKxXvylBl8hR7\nMF+35ncqb1zxFKsagP7ovuJ3dwLCpu/x3c83hWAd5n3O35Ulst6Rd32frisusvHB\ncv1ZsHlPji4IFrEJSsOSaReMAcxC5x2siDwefcUB4l3S8qdLvEweRNNxYOZ1Prl1\n8P/w4xvnfwKBgC5mHYYnUbxdV3obx70JQrsbsUWMQhnSa8k5jrPHFPtBrjhFaIc+\nPqjgOGIecTtqp60mJD4xFQr6byzpATrxEBExKlrqsttstxzsRHt8QcdnDqAjK90S\ntytuiUWG0Ufuqf8zKIJsNJwTZ+hNv9QXOxBdv2ureeM01TiiZMgA0iPhAoGAV1rd\nQazOi4oy2g+QiV804tReSkkrnASgLHab4Anmwt7P7Q92CJDG3KGlp78WdfKAx3iW\nJVYYNSOgbxjGCoFNj2pWFq7+9HZdnSjHQgT+AQsZiOMAcyK12JDr9dZiU6RurTQL\n9acI86pxBbutwV8SYKjXpsSQM29Gu0ktcoM/iT0CgYEAx58S2w4hXwd1g1i+dAPx\n0x08UMPrrxd1qObZW+1p178YyWdu6W5d9yBN5T/s9JXLvmwFHukWnRJEG3nWkJkC\nuSlso/Sgujm6p4OmD1NfIxOJ5hTqG0tiBXmcwSORx8sE61C+OjZ+1ETuaLXTzIA/\n0GbXSvsr7LudxN3B6ni81d8=\n-----END PRIVATE KEY-----\n",
"client_email": "fluttersheets@gsheets-374213.iam.gserviceaccount.com",
"client_id": "106640306727194774322",
"auth_uri": "https://accounts.google.com/o/oauth2/auth",
"token_uri": "https://oauth2.googleapis.com/token",
"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/fluttersheets%40gsheets-374213.iam.gserviceaccount.com"}''';
  static Worksheet? _userSheet;
  static final _gsheets = GSheets(_sheetCredentials);
  static Worksheet? _userSheet1;
  static final _gsheets1 = GSheets(_sheetCredentials);
  static final StreamController<CountPosition> _dataPositionObs =
      StreamController<CountPosition>.broadcast();
  static StreamController<CountPosition> get dataPositionObs =>
      _dataPositionObs;
  static final StreamController<DataSn> _dataQueryUpdate =
      StreamController<DataSn>.broadcast();
  static StreamController<DataSn> get dataQueryUpdate => _dataQueryUpdate;
  static Future init() async {
    try {
      final spreadsheet = await _gsheets.spreadsheet(_sheetId);
      _userSheet = await _getWorkSheet(spreadsheet, title: "Feed");
      _userSheet1 = await _getWorkSheet(spreadsheet, title: "Update");
      final firstRow = sheetscolumn.getColumns();
      print('test11 ===> $_userSheet');
      print('test12 ===> $_userSheet1');
//_userSheet!.values.insertRow(1, firstRow);
// _userSheet1!.values.insertRow(1, firstRow);
    } catch (e) {
      print(e);
    }
  }

  static Future<Worksheet> _getWorkSheet(
    Spreadsheet spreadsheet, {
    required String title,
  }) async {
    try {
      return await spreadsheet.addWorksheet(title);
    } catch (e) {
      return spreadsheet.worksheetByTitle(title)!;
    }
  }

  static Future insert(List<Map<String, dynamic>> rowList) async {
    _userSheet!.values.map.appendRows(rowList);
  }

  static Future update(String ID, List<Map<String, dynamic>> rowList) async {
    _userSheet1!.values.map.insertRowByKey(ID, rowList[0]);
  }

  static Future<String> testqq() async {
    Future.delayed(const Duration(seconds: 5));
    print('test ===> $_userSheet1');
    List<String> a = await _userSheet1!.values.row(5);
    final b = await _userSheet1?.values.allRows();

    DataSheets dataSheets = DataSheets();
    for (List<String> item in b!) {
      print('items ===> $item');
      dataSheets.listData
          ?.add(ItemDataSheets(ID: item[0], position: item[1], Can: item[2]));
    }
    dataSheets.listData?.removeAt(0);
    final numRfs = dataSheets.listData
        ?.where((element) => element.position == 'RFS')
        .toList();
    final numCompany = dataSheets.listData
        ?.where((element) => element.position == 'Outsource')
        .toList();
    final numPps = dataSheets.listData
        ?.where((element) => element.position == 'Logistics')
        .toList();
    final numOther = dataSheets.listData
        ?.where((element) =>
            element.position == 'Zone A' ||
            element.position == 'Zone B' ||
            element.position == 'Zone C' ||
            element.position == 'Zone D' ||
            element.position == 'Zone E' ||
            element.position == 'Zone F' ||
            element.position == 'Zone H' ||
            element.position == 'Zone J' ||
            element.position == 'Zone K' ||
            element.position == 'Zone L' ||
            element.position == 'Zone N' ||
            element.position == 'Zone O' ||
            element.position == 'Zone Q' ||
            element.position == 'Check Up Center' ||
            element.position == 'หน่วยตรวจสุขภาพ' ||
            element.position == 'Dental Care Center' ||
            element.position == 'ศูนย์ทันตกรรม' ||
            element.position == 'Zone R' ||
            element.position == 'Zone S' ||
            element.position == 'Zone T' ||
            element.position == 'Zone U' ||
            element.position == 'Zone V' ||
            element.position == 'Endoscopy Center' ||
            element.position == 'ศูนย์ส่องกล้อง' ||
            element.position == 'Short stay' ||
            element.position == 'หน่วยบำบัดระยะสั้น' ||
            element.position == 'Hemodialysis' ||
            element.position == 'หน่วยไตเทียม' ||
            element.position == 'ICU 51' ||
            element.position == 'Chemotherapy Service' ||
            element.position == 'หน่วยบริการเภสัชกรรมด้านยาเคมีบำบัด' ||
            element.position == 'LR' ||
            element.position == 'ห้องคลอด' ||
            element.position == 'Nursery' ||
            element.position == 'หน่วยบริบาลทารกแรกเกิด' ||
            element.position == 'Ward 62' ||
            element.position == 'Ward 63' ||
            element.position == 'Ward 64' ||
            element.position == 'Ward 65' ||
            element.position == 'NICU' ||
            element.position == 'Sleep Lab' ||
            element.position == 'Ward 71' ||
            element.position == 'Ward 72' ||
            element.position == 'Ward 73' ||
            element.position == 'Ward 74' ||
            element.position == 'Ward 75' ||
            element.position == 'Ward 76' ||
            element.position == 'Rehabilitation Center' ||
            element.position == 'ศูนย์เวชกรรมฟื้นฟู' ||
            element.position == 'Ward 81' ||
            element.position == 'Ward 82' ||
            element.position == 'Ward 83' ||
            element.position == 'Ward 84' ||
            element.position == 'Ward 85' ||
            element.position == 'Ward 86' ||
            element.position == 'CCU 91' ||
            element.position == 'ICU 92' ||
            element.position == 'ICU 95' ||
            element.position == 'Ward 96' ||
            element.position ==
                'Ambulatory ECG Monitoring Center&Fluoroscopy' ||
            element.position ==
                'ศูนย์ติดตามคลื่นไฟฟ้าหัวใจและหัตถการฟลูโอโรสโคปี้' ||
            element.position == 'Presidential Suite' ||
            element.position == 'ห้องพักพิเศษ' ||
            element.position == 'Royal Suite ' ||
            element.position == 'ที่ประทับส่วนพระองค์')
        .toList();
    CountPosition countPosition = CountPosition(
        numRfs: numRfs?.length ?? 0,
        company: numCompany?.length ?? 0,
        pps: numPps?.length ?? 0,
        other: numOther?.length ?? 0,
        itemsDataSheet: numOther);
    _dataPositionObs.sink.add(countPosition);
    print('testDataSheets ====> ${dataSheets.listData?.length}');
    print(b);
    print(a);
    return '${a[0]} - ${a[1]} - ${a[2]}';
  }

  static void getDataQueryTableUpdate(String param) async {
    final data = await _userSheet1?.values.rowByKey(param);
    print(data);
    _dataQueryUpdate.sink
        .add(DataSn(position: data![0], typeCy: data[1], Instru: data[4]));
  }
}

class DataSheets {
  String? ID;
  String? position;
  String? Can;
  List<ItemDataSheets>? listData = [];
  DataSheets();
  DataSheets.fromJson(List<dynamic> parsedJson) {
    for (int i = 0; i < parsedJson.length; i++) {
      if (i == 0) {
        ID = parsedJson[0];
      } else if (i == 1) {
        position = parsedJson[1];
      } else {
        Can = parsedJson[2];
      }
      listData?.add(ItemDataSheets(ID: ID, position: position, Can: Can));
    }
  }
}

class ItemDataSheets {
  String? ID;
  String? position;
  String? Can;
  ItemDataSheets({required this.ID, required this.position, required this.Can});
}

class CountPosition {
  int? numRfs = 0;
  int? company = 0;
  int? pps = 0;
  int? other = 0;
  List<ItemDataSheets>? itemsDataSheet;
  CountPosition(
      {this.numRfs, this.company, this.pps, this.other, this.itemsDataSheet});
}

class DataSn {
  final String position;
  final String typeCy;
  final String Instru;
  DataSn({required this.position, required this.typeCy, required this.Instru});
}
