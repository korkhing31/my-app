class sheetscolumn {
  static const ID = "ID";
  static const location = "location";
  static const Instrument = "Instrument";

  static List<String> getColumns() => [ID, location, Instrument];
}

class Datash {
  final String ID;
  final String location;
  final String Instrument;

  const Datash({required this.ID, required this.location, required this.Instrument});

  Datash copy({String? ID, String? location, String? Instrument}) => Datash(
      ID: ID ?? this.ID,
      location: location ?? this.location,
      Instrument: Instrument ?? this.Instrument);
  static Datash fromJson(Map<String, dynamic> json) => Datash(
      ID: json[sheetscolumn.ID],
      location: json[sheetscolumn.location],
      Instrument: json[sheetscolumn.Instrument]);

  Map<String, dynamic> toJson() => {
        sheetscolumn.ID: ID,
        sheetscolumn.location: location,
        sheetscolumn.Instrument: Instrument
      };
}