import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medicalgas/scanner/qr_scan.dart';

import 'googlesheets.dart';

class Sndata extends StatefulWidget {
  final String qrData;
  Sndata({
    required this.qrData,
  });

  @override
  State<Sndata> createState() => _SndataState();
}

class _SndataState extends State<Sndata> {
  @override
  void initState() {
    SheetsFlutter.getDataQueryTableUpdate(widget.qrData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("ตำแหน่งปัจุบัน",
              style:
                  GoogleFonts.mitr(fontSize: 20, fontWeight: FontWeight.w400))),
      body: StreamBuilder<DataSn>(
        stream: SheetsFlutter.dataQueryUpdate.stream,
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return const Center(
                child: CupertinoActivityIndicator(
              color: Colors.grey,
            ));
          } else {
            return Column(
              children: [
                /////////////////////////////เลข Sn/////////////////////////////////
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 80,
                        width: 375,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0, 2),
                                blurRadius: 6.0,
                              ),
                            ],
                            color: Colors.green[200],
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          children: [
                            Text(
                              ' SN : ${widget.qrData}',
                              style: GoogleFonts.mitr(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                ////////////////////// ตำแหน่งปจบ.////////////////
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 80,
                        width: 375,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0, 2),
                                blurRadius: 6.0,
                              ),
                            ],
                            color: Colors.green[200],
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          children: [
                            Text(
                              ' ตำแหน่ง : ${snapshot.data!.position}',
                              style: GoogleFonts.mitr(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                ///////////////////////////////ประเภทgas////////////////
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 80,
                        width: 375,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0, 2),
                                blurRadius: 6.0,
                              ),
                            ],
                            color: Colors.green[200],
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                ' ประเภท : ${snapshot.data!.typeCy}',
                                style: GoogleFonts.mitr(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
//////////////////////////////////////////// Instrument เพิ่มเติม ////////////////////////////////////
 Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 80,
                        width: 375,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0, 2),
                                blurRadius: 6.0,
                              ),
                            ],
                            color: Colors.green[200],
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          children: [
                            Text(
                              ' อุปกรณ์เพิ่มเติม: ${snapshot.data!.Instru}',
                              style: GoogleFonts.mitr(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),


              ],
            );
          }
        },
      ),
    );
  }
}
