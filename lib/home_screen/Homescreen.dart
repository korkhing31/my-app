import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medicalgas/scanner/qr_scan.dart';
import 'package:medicalgas/data/data.dart';
import 'package:medicalgas/test/testgsheet.dart';
import '../adddata/googlesheets.dart';


class Homescreen extends StatelessWidget {
  const Homescreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Medical Gas",
      home: Myhomepage(),
      theme: ThemeData(primarySwatch: Colors.green),
    );
  }
}

//widget แบบกดได้
class Myhomepage extends StatefulWidget {
  const Myhomepage({super.key});

  @override
  State<Myhomepage> createState() => _MyhomepageState();
}

class _MyhomepageState extends State<Myhomepage> {

@override
  void initState(){
    SheetsFlutter.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Image(
              image: AssetImage('assets/images/Medgasbiglogo.png'),
              alignment: Alignment.topCenter,
              height: 300,
              width: 300,
            ),
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            alignment: Alignment.center,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 6.0,
                minimumSize: Size(350, 100),
                primary: Colors.blue[900],
                onPrimary: Colors.white,
              ),
              child: Text(
                'ดูฐานข้อมูล',
                style:
                    GoogleFonts.mitr(fontSize: 25, fontWeight: FontWeight.w500),
              ),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: ((context) => data())));
              },
            ),
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            alignment: Alignment.center,
            child: ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                elevation: 6.0,
                minimumSize: Size(350, 100),
                primary: Colors.blue[900],
                onPrimary: Colors.white,
              ),
              icon: Icon(Icons.qr_code_scanner_rounded),
              label: Text(
                'แสกนQRCODE',
                style:
                    GoogleFonts.mitr(fontSize: 25, fontWeight: FontWeight.w500)
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: ((context) => scanner())));
              },
            ),
          ),
        ],
      ),
    );
  }
}
