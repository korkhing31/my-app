import 'package:flutter/material.dart';
import 'package:medicalgas/adddata/googlesheets.dart';
import 'location_model.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';


class datacard extends StatefulWidget {
  final List<ItemDataSheets> itemsDataSheet;
  const datacard({super.key, required this.itemsDataSheet});
 
 
 
  @override
  State<datacard> createState() => _datacard();
}

class _datacard extends State<datacard> {
//กลุ่มข้อมูล
 static List<Wardlocation> location = [
    Wardlocation("Zone A", "หน่วยตรวจผู้ป่วยนอกอายุรกรรม", 1),
    Wardlocation("Zone B", "หน่วยตรวจผู้ป่วยนอกอายุรกรรม", 1),
    Wardlocation("Zone C", "หน่วยตรวจผู้ป่วยนอกอายุรกรรม", 1),
    Wardlocation("Zone D", "หน่วยตรวจผู้ป่วยนอกศัลยกรรม", 1),
    Wardlocation("Zone E", "หน่วยตรวจผู้ป่วยนอกศัลยกรรม", 1),
    Wardlocation("Zone F", "หน่วยตรวจผู้ป่วยนอกศัลยกรรม", 1),
    Wardlocation("Zone H", "หน่วยตรวจผู้ป่วยนอกโสต ศอ นาสิก", 2),
    Wardlocation("Zone J", "หน่วยตรวจผู้ป่วยนอกผิวหนัง", 2),
    Wardlocation("Zone K", "หน่วยตรวจผู้ป่วยนอกศัลยกรรมกระดูก", 2),
    Wardlocation("Zone L", "หน่วยตรวจผู้ป่วยนอกพิเศษสุขภาพสตรี", 3),
    Wardlocation("Zone N", "หน่วยตรวจผู้ป่วยนอกพิเศษสุขภาพสตรี", 3),
    Wardlocation("Zone O", "หน่วยตรวจผู้ป่วยนอกพิเศษสุขภาพสตรี", 3),
    Wardlocation("Zone Q", "หน่วยตรวจผู้ป่วยนอกพิเศษโสต ศอ นาสิก", 3),
    Wardlocation("Check Up Center", "หน่วยตรวจสุขภาพ", 4),
    Wardlocation("Dental Care Center", "ศูนย์ทันตกรรม", 4),
    Wardlocation("Zone R", "หน่วยตรวจผู้ป่วยนอกพิเศษ-เวชสำอาง", 4),
    Wardlocation("Zone S", "หน่วยตรวจผู้ป่วยนอกพิเศษจักษุ", 4),
    Wardlocation("Zone T", "หน่วยตรวจผู้ป่วยนอกพิเศษศัลยกรรมและกระดูก", 4),
    Wardlocation("Zone U", "หน่วยตรวจผู้ป่วยนอกพิเศษอายุรกรรม", 4),
    Wardlocation("Zone V", "หน่วยตรวจผู้ป่วยนอกพิเศษอายุรกรรม", 4),
    Wardlocation("Endoscopy Center", "ศูนย์ส่องกล้อง", 5),
    Wardlocation("Short stay", "หน่วยบำบัดระยะสั้น", 5),
    Wardlocation("Hemodialysis", "หน่วยไตเทียม", 5),
    Wardlocation("ICU 51", "หอผู้ป่วยวิกฤต 5", 5),
    Wardlocation(
        "Chemotherapy Service", "หน่วยบริการเภสัชกรรมด้านยาเคมีบำบัด", 6),
    Wardlocation("LR", "ห้องคลอด", 6),
    Wardlocation("Nursery", "หน่วยบริบาลทารกแรกเกิด", 6),
    Wardlocation("Ward 62", "หอผู้ป่วยพิเศษ 62", 6),
    Wardlocation("Ward 63", "หอผู้ป่วยพิเศษ 63", 6),
    Wardlocation("Ward 64", "หอผู้ป่วยพิเศษ 64", 6),
    Wardlocation("Ward 65", "หอผู้ป่วยพิเศษ 65", 6),
    Wardlocation("NICU", "หอผู้ป่วยวิกฤตทารกแรกเกิด", 6),
    Wardlocation("Sleep Lab", "ศูนย์ตรวจโรคการนอนหลับ", 7),
    Wardlocation("Ward 71", "หอผู้ป่วยพิเศษ 71", 7),
    Wardlocation("Ward 72", "หอผู้ป่วยพิเศษ 72", 7),
    Wardlocation("Ward 73", "หอผู้ป่วยพิเศษ 73", 7),
    Wardlocation("Ward 74", "หอผู้ป่วยพิเศษ 74", 7),
    Wardlocation("Ward 75", "หอผู้ป่วยพิเศษ 75", 7),
    Wardlocation("Ward 76", "หอผู้ป่วยพิเศษ 76", 7),
    Wardlocation("Rehabilitation Center", "ศูนย์เวชกรรมฟื้นฟู", 7),
    Wardlocation("Ward 81", "หอผู้ป่วยพิเศษ 81", 8),
    Wardlocation("Ward 82", "หอผู้ป่วยพิเศษ 82", 8),
    Wardlocation("Ward 83", "หอผู้ป่วยพิเศษ 83", 8),
    Wardlocation("Ward 84", "หอผู้ป่วยพิเศษ 84", 8),
    Wardlocation("Ward 85", "หอผู้ป่วยพิเศษ 85", 8),
    Wardlocation("Ward 86", "หอผู้ป่วยพิเศษ 86", 8),
    Wardlocation("Biomedical Engineering", "วิศวกรรมชีวการแพทย์", 8),
    Wardlocation("CCU 91", "หอผู้ป่วยวิกฤตหัวใจ 91", 9),
    Wardlocation("ICU 92", "หอผู้ป่วยวิกฤต 92", 9),
    Wardlocation("ICU 95", "หอผู้ป่วยวิกฤต 95", 9),
    Wardlocation("Ward 96", "หอผู้ป่วยพิเศษ 96", 9),
    Wardlocation("Ambulatory ECG Monitoring Center&Fluoroscopy",
        "ศูนย์ติดตามคลื่นไฟฟ้าหัวใจและหัตถการฟลูโอโรสโคปี้", 9),
    Wardlocation("Presidential Suite", "ห้องพักพิเศษ", 9),
    Wardlocation("Royal Suite", "ที่ประทับส่วนพระองค์", 9),
  ];

  List<Wardlocation> display_location = List.from(location);

  void updateList(String value) {
    setState(() {
      display_location = location
          .where((element) =>
              element.title.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        "ระบุตำแหน่ง",
        style: TextStyle(fontSize: 30),
      )),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20.0,
            ),
            TextField(
              onChanged: (value) => updateList(value),
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey[350],
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide.none,
                ),
                hintText: "เช่น : Ward 82",
                prefixIcon: Icon(Icons.search),
                prefixIconColor: Colors.white,
              ),
            ),
            SizedBox(height: 20.0),
            Expanded(
              child: display_location.length == 0
                  ? Center(
                      child: Text(
                      "ไม่พบสถานที่ที่ระบุ",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ))
                  : ListView.builder(
                      itemCount: display_location.length,
                      itemBuilder: (BuildContext, int index) {
                        Wardlocation Ward = location[index];
                        return Card(
                          elevation: 8,
                          margin: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5),
                          child: ListTile(
                            leading: CircleAvatar(
                                radius: 20,
                                child: FittedBox(
                                  child: Text(Ward.floor.toString(),
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                )),
                            title: Text(
                              display_location[index].title,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            subtitle: Text(
                              display_location[index].name,
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        );
                      }),
            ),
          ],
        ),
      ),
    );
  }
}
