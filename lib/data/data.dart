import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medicalgas/datacard/datacard.dart';
import '../adddata/googlesheets.dart';



class data extends StatefulWidget {
  const data({super.key});

  @override
  State<data> createState() => _dataState();
}

class _dataState extends State<data> {

  String _value = '';

@override 
  void initState() {
    super.initState();
    
    getValue();
  }
  

  void getValue () async {
    Timer(Duration(seconds: 5), () async {
      final values = await SheetsFlutter.testqq();
      setState(()  {
      _value = values;
    });
    });
  }

  @override
  Widget build(BuildContext context) {
    ////////    app bar ////////////////
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          elevation: 10,
          title: Text('ฐานข้อมูล',
              style:
                  GoogleFonts.mitr(fontSize: 20, fontWeight: FontWeight.w500)),
          centerTitle: true,
        ),

        //////////// rfs /////////////

        body: StreamBuilder<CountPosition>(
          stream: SheetsFlutter.dataPositionObs.stream,
          builder: (context, snapshot){
              if(snapshot.data == null){
                return const Center(
                  child: CupertinoActivityIndicator(
                    color: Colors.grey,
                  )
                );
              }else{
                return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(7.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(7.0),
                    child: Container(
                      height: 175,
                      width: 375,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 2),
                              blurRadius: 6.0,
                            ),
                          ],
                          color: Colors.green[100],
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Column(
                        children: [
                          Text('RFS',
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                          Text('จำนวน',
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                          Text(snapshot.data!.numRfs.toString(),
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                  ),
        
                  //////////// company  /////////////
        
                  Padding(
                    padding: const EdgeInsets.all(7.0),
                    child: Container(
                      height: 175,
                      width: 375,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 2),
                              blurRadius: 6.0,
                            ),
                          ],
                          color: Colors.green[200],
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Column(
                        children: [
                          Text(' Outsource (ชั้นB1)',
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                          Text('จำนวน',
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                          Text(snapshot.data!.company.toString(),
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                  ),
        
                   //////////////////////// เวรเปล  /////////////////////////
        
                  Padding(
                    padding: const EdgeInsets.all(7.0),
                    child: Container(
                      height: 175,
                      width: 375,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 2),
                              blurRadius: 6.0,
                            ),
                          ],
                          color: Colors.green[300],
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Column(
                        children: [
                          Text('Logistics',
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500,)),
                          Text('จำนวน',
                              style: GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                          Text(snapshot.data!.pps.toString(),
                              style: 
                              GoogleFonts.mitr(
                                  fontSize: 25, fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                  ),
        
                  //////////// แผนกทั้งหมด /////////////
        
                  Padding(
                    padding: const EdgeInsets.all(7.0),
                    child: Column(
                      children: [
                        Container(
                          height: 160,
                          width: 375,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0, 2),
                                  blurRadius: 6.0,
                                ),
                              ],
                              color: Colors.green[400],
                              borderRadius:BorderRadius.vertical(top: Radius.circular(20))),
                          child: Column(
                            children: [
                              Text('แผนกทั้งหมด',
                                  style: GoogleFonts.mitr(
                                      fontSize: 25, fontWeight: FontWeight.w500)),
                              Text('จำนวน',
                                  style: GoogleFonts.mitr(
                                      fontSize: 25, fontWeight: FontWeight.w500)),
                              Text(snapshot.data!.other.toString(),
                                  style: GoogleFonts.mitr(
                                      fontSize: 25, fontWeight: FontWeight.w500)),
                            ],
                          ),//text
                        ),
                        
                        Container(
              alignment: Alignment.center,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 8.0,
                  shadowColor: Color.fromARGB(0, 62, 62, 62),
                  minimumSize: Size(400, 50),
                  primary: Colors.blue,
                  onPrimary: Colors.white,  
                  
                ),
                child: Text(
                  'ดูแผนกต่างๆ',
                  style:
                      GoogleFonts.mitr(fontSize: 20, fontWeight: FontWeight.w500),
                ),
                onPressed: () {Navigator.push(context,
                 MaterialPageRoute(builder: ((context) => datacard(itemsDataSheet: snapshot.data!.itemsDataSheet!,))
                ));
                },
              ),
            ),
                      ],
                    ),
                    
                  ),
                ],
              ),
            ),
          ); 
              }
          }, 
        ));
  }
}
