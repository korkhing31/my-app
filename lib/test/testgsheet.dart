import 'dart:async';

import 'package:gsheets/gsheets.dart';
import 'package:flutter/material.dart';
import 'package:medicalgas/adddata/googlesheets.dart';


const _credentials =  r'''
{
  "type": "service_account",
  "project_id": "gsheets-374213",
  "private_key_id": "af6aa4c3e7e561834da90e28647a0b3eeb876359",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCxKu+AZWTw4VJh\nQR7LYE4PuNC1+pmK5xYuh0I2zxJXDPn16VwDmQ9y7JkFs5XHsG5vI3TbU+BZXc3C\nBLHcY6hgXREakbVyrZOnBKH7omwUJN+aOAHBEtQnMyQlAHW4le+jabDMoXLERdYx\n1EEn6kdsOs67ixeIOJ2X8jO3FUZkWO+3pzbi+0QrhadzoUCcW5ZW5u4uHl4kFS9Z\nDOrvvK+eMO389YFt3kZd1nXRhmZvdaw0yA0Pp0JACM0wKBsHHbaVVaRPnR+IpITu\nWTLoV/WagYRJ5KiGKF6iAc67Kxntlx2+8otOoIXZvs6ABcTj6x/hVgd8MPqdSBPz\nFEB1gkxXAgMBAAECggEAK3NVQ88aP31zX8jBza0z1+VCwq7QQ2HDbevcUGben5ze\nHPJ7j6JVSKFAMkbGADCEFRvECBp9Fk3MLb5LBLsoS2Hn1PVRY+uwH7EiLP27tgEJ\nGeCMhZahk2+WeMCxHnEfUZuVMcqSxBJAov3kcO6jT6boltRZFB6Wf/J1vnwoRGmm\n/oXqqIhDv5TO8UlRgdtsqO9C7SfzropBjmrCFPlJgx+7I6HKTJCcazSS0oKP9y0G\n9/1vyn4EnawcEn5kUktDIHwOhulu6xEwyzbK6Ui6ec41aMMkJUmRKknFByRMY3JM\ndn8qI/TtpV7Ijy/2lYPLmg6g+s7/2paaybO7PzLvMQKBgQDYxQRifPf27npVLH0h\nq3cjOozoFb8kzMkZksuc1G9KNqDrszjNrsoKDZmzelS11UfAmVGd1+knMJUMMZqb\nO7F/HCskhIWMzZsAkpp09Ff0e1QmkzwUtIaSiSyHe9qaUHpNw0Lk27PxQUJGnkJ1\n8h17vpdGfq2P/9yfkGnjoipHKQKBgQDROyeUwvthoL2OSmLYkjzCKxXvylBl8hR7\nMF+35ncqb1zxFKsagP7ovuJ3dwLCpu/x3c83hWAd5n3O35Ulst6Rd32frisusvHB\ncv1ZsHlPji4IFrEJSsOSaReMAcxC5x2siDwefcUB4l3S8qdLvEweRNNxYOZ1Prl1\n8P/w4xvnfwKBgC5mHYYnUbxdV3obx70JQrsbsUWMQhnSa8k5jrPHFPtBrjhFaIc+\nPqjgOGIecTtqp60mJD4xFQr6byzpATrxEBExKlrqsttstxzsRHt8QcdnDqAjK90S\ntytuiUWG0Ufuqf8zKIJsNJwTZ+hNv9QXOxBdv2ureeM01TiiZMgA0iPhAoGAV1rd\nQazOi4oy2g+QiV804tReSkkrnASgLHab4Anmwt7P7Q92CJDG3KGlp78WdfKAx3iW\nJVYYNSOgbxjGCoFNj2pWFq7+9HZdnSjHQgT+AQsZiOMAcyK12JDr9dZiU6RurTQL\n9acI86pxBbutwV8SYKjXpsSQM29Gu0ktcoM/iT0CgYEAx58S2w4hXwd1g1i+dAPx\n0x08UMPrrxd1qObZW+1p178YyWdu6W5d9yBN5T/s9JXLvmwFHukWnRJEG3nWkJkC\nuSlso/Sgujm6p4OmD1NfIxOJ5hTqG0tiBXmcwSORx8sE61C+OjZ+1ETuaLXTzIA/\n0GbXSvsr7LudxN3B6ni81d8=\n-----END PRIVATE KEY-----\n",
  "client_email": "fluttersheets@gsheets-374213.iam.gserviceaccount.com",
  "client_id": "106640306727194774322",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/fluttersheets%40gsheets-374213.iam.gserviceaccount.com"
}
''';
 
 const _spreadsheetId =  "1b-jGpDBouTt6bF0iOL7U2pTfCMkFd4XY143GxM451yc";
 
  


class test extends StatefulWidget {
  const test({super.key});

  @override
  State<test> createState() => _testState();
}

class _testState extends State<test> {
  var _value = ''; 

  @override 
  void initState() {
    super.initState();
    SheetsFlutter.init();
    getValue();
  }

  void getValue () async {
    Timer(Duration(seconds: 5), () async {
      final values = await SheetsFlutter.testqq();
      setState(()  {
      _value = values;
    });
    });
    
  }

 void  init() async {
    final gsheets = GSheets(_credentials);
    print('test =====> $gsheets');
    final ss = await gsheets.spreadsheet(_spreadsheetId);
    var sheet = ss.worksheetByTitle('Feed');
    var value = await sheet?.values.value(column: 5, row: 1);
    
    setState(() {
    _value = value??'';
    print('test$_value');   
    });

}

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.green,
          ),
          child: Text('${_value}'),
        ),
      ),
    );
  }
}