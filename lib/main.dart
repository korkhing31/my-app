import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:medicalgas/home_screen/homescreen.dart';

Future<void> main() async { 
  WidgetsFlutterBinding.ensureInitialized();
  var app = Medicalgas();
  runApp(app);
}

class Medicalgas extends StatelessWidget {
  const Medicalgas({super.key});

  //test update

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'home_screen',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      
      home: AnimatedSplashScreen(
      duration: 2250,
            splash:Image(image: AssetImage('assets/images/Medgasbiglogo.png',)),
            nextScreen: Homescreen(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor: Colors.white));
  }
}
