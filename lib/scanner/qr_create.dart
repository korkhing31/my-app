
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class qrcreate extends StatefulWidget {
  const qrcreate({super.key});

  @override
  State<qrcreate> createState() => _qrcreateState();
}

class _qrcreateState extends State<qrcreate> {
  final controller = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          elevation: 10,
          title: Text('สร้าง Qr Code',
              style:
                  GoogleFonts.mitr(fontSize: 20, fontWeight: FontWeight.w500)),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 15),
                    QrImage(data: controller.text,
                    size: 200,
                    backgroundColor:Colors.white,
                    ),
                    buildTextField(context),
                  ],
                ),
              ),
            ],
          ),
        ));    
  }


   Widget buildTextField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: TextField(
          controller: controller,
          style: GoogleFonts.mitr(fontSize: 10, fontWeight: FontWeight.w500),
          decoration: InputDecoration(
            hintText: 'ใส่ข้อมูล ',
            hintStyle: TextStyle(color: Colors.grey),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(
                color: Theme.of(context).accentColor,
              ),
            ),
            suffixIcon: IconButton(
              color: Theme.of(context).accentColor,
              icon: Icon(Icons.done, size: 20),
              onPressed: () => setState(() {}),
            ),
          ),
        ),
    );
  }
}
