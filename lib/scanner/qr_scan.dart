import 'dart:io';
import 'package:flutter/material.dart';
import 'package:medicalgas/adddata/Sndata.dart';
import 'package:medicalgas/adddata/adddata.dart';
import 'package:medicalgas/scanner/qr_create.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class scanner extends StatefulWidget {
  const scanner({super.key});

  @override
  State<scanner> createState() => _scannerState();
}

class _scannerState extends State<scanner> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Expanded(
          flex: 4,
          child: QRView(
            key: qrKey,
            onQRViewCreated: _onQRViewCreated,
            overlay: QrScannerOverlayShape(
              borderColor: Colors.white,
              borderRadius: 10,
              borderLength: 20,
              borderWidth: 10,
              cutOutSize: MediaQuery.of(context).size.width * 0.8,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Center(
            child: (result != null)
                ? Text(
                    ' Data: ${result!.code}')
                : Text(
                    'Scan a code',
                    style: GoogleFonts.mitr(
                      fontSize: 25,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
          ),
        ),

        /////////////// flash + resume//////////////
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.green[100],
                    borderRadius: BorderRadius.circular(6)),
                margin: const EdgeInsets.all(20),
                child: ElevatedButton.icon(
                  icon: Icon(Icons.flashlight_on_rounded),
                  onPressed: () async {
                    await controller?.toggleFlash();
                  },
                  label: Text('flash',
                      style: GoogleFonts.mitr(
                          fontSize: 20, fontWeight: FontWeight.w400)),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.green[100],
                    borderRadius: BorderRadius.circular(6)),
                margin: const EdgeInsets.all(15),
                child: ElevatedButton.icon(
                  icon: Icon(
                    Icons.camera_alt_rounded,
                    size: 25,
                  ),
                  onPressed: () async {
                    await controller?.resumeCamera();
                  },
                  label: Text('เปิดกล้อง',
                      style: GoogleFonts.mitr(
                          fontSize: 20, fontWeight: FontWeight.w400)),
                ),
              ),
            ]),
///////////////////////////////////////////
        Row(
          children: [
            Expanded(
                flex: 5,
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                  elevation: 6.0,
                  minimumSize: Size(50, 25),
                ),
                  icon: Icon(
                    Icons.add_circle_sharp,
                    size: 25,
                  ),
                  onPressed: () async {
                    if (result != null) {
                      print('test1 ${result?.code}'); 
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => adddata(qrData: result!.code??'',),
                      )).then((value) async {
                        // await controller!.resumeCamera();
                      });
                      // await controller!.stopCamera();
                    }
                  },
                  label: Text('เพิ่มข้อมูล',
                      style: GoogleFonts.mitr(
                          fontSize: 20, fontWeight: FontWeight.w400)),
                )),
          ],
        ),
        ////////////// เช็ค เลขsn ///////////////
        Row(
          children: [
            Expanded(
                flex: 5,
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                  elevation: 6.0,
                  minimumSize: Size(50, 25),
                ),
                  icon: Icon(
                    Icons.check_circle_sharp,
                    size: 25,
                  ),
                  onPressed: () {
                    if (result != null) {
                      print('test1 ${result?.code}'); 
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Sndata(qrData: result!.code??'',),
                      ));
                    }
                  },
                  label: Text('ดูตำแหน่งปัจุบัน',
                      style: GoogleFonts.mitr(
                          fontSize: 20, fontWeight: FontWeight.w400)),
                )),
          ],
        ),

        ///////////////สร้างQRCODE//////////////

        Row(
          children: [
            Expanded(
              flex: 3,
              child: ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                  elevation: 6.0,
                  minimumSize: Size(50, 30),
                ),
                icon: Icon(
                  Icons.qr_code_rounded,
                  size: 50,
                ),
                label: Text(
                  'สร้าง QRCODE',
                  style: GoogleFonts.mitr(
                      fontSize: 25, fontWeight: FontWeight.w500),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: ((context) => qrcreate())));
                },
              ),
            ),
          ],
        ),
      ]),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}