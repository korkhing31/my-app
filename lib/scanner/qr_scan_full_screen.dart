import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrScanFullScreen extends StatefulWidget {
  const QrScanFullScreen({Key? key}) : super(key: key);

  @override
  State<QrScanFullScreen> createState() => _QrScanFullScreenState();
}

class _QrScanFullScreenState extends State<QrScanFullScreen> {
  final GlobalKey qrKeys = GlobalKey(debugLabel: 'QR');
  QRViewController? controllers;


  @override
  void initState() {
    _openCamera();
    super.initState();
  }


  @override
  void dispose() {
    // controllers?.stopCamera();

    super.dispose();
  }

  void _openCamera() {
    Timer(const Duration(milliseconds: 300),  () async => await controllers?.resumeCamera());
  }

  // @override
  // void reassemble() {
  //   super.reassemble();
  //   if (Platform.isAndroid) {
  //     controllers!.pauseCamera();
  //   } else if (Platform.isIOS) {
  //     controllers!.resumeCamera();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: QRView(
        key: qrKeys,
        onQRViewCreated: _onQRViewCreated,
        overlay: QrScannerOverlayShape(
          borderColor: Colors.white,
          borderRadius: 10,
          borderLength: 20,
          borderWidth: 10,
          cutOutSize: MediaQuery.of(context).size.width * 0.8,
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controllers) {
    this.controllers = controllers;
    controllers.scannedDataStream.listen((scanData) {
      Navigator.of(context).pop({'qrData' : scanData.code});
      controllers.dispose();
    });
  }
}
